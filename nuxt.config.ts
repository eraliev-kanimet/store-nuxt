export default defineNuxtConfig({
    runtimeConfig: {
        public: {
            appUrl: process.env.NUXT_PUBLIC_APP_URL,
            apiUrl: process.env.NUXT_PUBLIC_API_URL,
        }
    },
    devtools: {enabled: !!process.env.NUXT_PUBLIC_APP_DEBUG},
    modules: [
        '@nuxtjs/tailwindcss',
        '@pinia/nuxt',
        'nuxt-icon',
    ],
    css: ['~/assets/scss/app.scss'],
    tailwindcss: {},
})
